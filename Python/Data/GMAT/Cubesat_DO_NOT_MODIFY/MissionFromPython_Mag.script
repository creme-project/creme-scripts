%General Mission Analysis Tool(GMAT) Script
%Created: 2020-11-05 15:34:02


%----------------------------------------
%---------- Spacecraft
%----------------------------------------

Create Spacecraft DefaultSat;
GMAT DefaultSat.DateFormat = UTCGregorian;
GMAT DefaultSat.Epoch = '01 Jan 2023 00:00:00.000';
GMAT DefaultSat.CoordinateSystem = EarthMJ2000Eq;
GMAT DefaultSat.DisplayStateType = Keplerian;
GMAT DefaultSat.SMA = 6978.14;
GMAT DefaultSat.ECC = 0;
GMAT DefaultSat.INC = 97.79;
GMAT DefaultSat.RAAN = 190.0961;
GMAT DefaultSat.AOP = 0;
GMAT DefaultSat.TA = 0;
GMAT DefaultSat.DryMass = 4;
GMAT DefaultSat.Cd = 2.2;
GMAT DefaultSat.Cr = 1;
GMAT DefaultSat.DragArea = 0.0362;
GMAT DefaultSat.SRPArea = 0.0362;
GMAT DefaultSat.SPADDragScaleFactor = 1;
GMAT DefaultSat.SPADSRPScaleFactor = 1;
GMAT DefaultSat.NAIFId = -10000001;
GMAT DefaultSat.NAIFIdReferenceFrame = -9000001;
GMAT DefaultSat.OrbitColor = Red;
GMAT DefaultSat.TargetColor = Teal;
GMAT DefaultSat.OrbitErrorCovariance = [ 1e+70 0 0 0 0 0 ; 0 1e+70 0 0 0 0 ; 0 0 1e+70 0 0 0 ; 0 0 0 1e+70 0 0 ; 0 0 0 0 1e+70 0 ; 0 0 0 0 0 1e+70 ];
GMAT DefaultSat.CdSigma = 1e+70;
GMAT DefaultSat.CrSigma = 1e+70;
GMAT DefaultSat.Id = 'SatId';
GMAT DefaultSat.Attitude = CoordinateSystemFixed;
GMAT DefaultSat.SPADSRPInterpolationMethod = Bilinear;
GMAT DefaultSat.SPADSRPScaleFactorSigma = 1e+70;
GMAT DefaultSat.SPADDragInterpolationMethod = Bilinear;
GMAT DefaultSat.SPADDragScaleFactorSigma = 1e+70;
GMAT DefaultSat.ModelFile = 'aura.3ds';
GMAT DefaultSat.ModelOffsetX = 0;
GMAT DefaultSat.ModelOffsetY = 0;
GMAT DefaultSat.ModelOffsetZ = 0;
GMAT DefaultSat.ModelRotationX = 0;
GMAT DefaultSat.ModelRotationY = 0;
GMAT DefaultSat.ModelRotationZ = 0;
GMAT DefaultSat.ModelScale = 1;
GMAT DefaultSat.AttitudeDisplayStateType = 'Quaternion';
GMAT DefaultSat.AttitudeRateDisplayStateType = 'AngularVelocity';
GMAT DefaultSat.AttitudeCoordinateSystem = EarthMJ2000Eq;
GMAT DefaultSat.EulerAngleSequence = '321';

























%----------------------------------------
%---------- ForceModels
%----------------------------------------

Create ForceModel Propagator1_ForceModel;
GMAT Propagator1_ForceModel.CentralBody = Earth;
GMAT Propagator1_ForceModel.PrimaryBodies = {Earth};
GMAT Propagator1_ForceModel.PointMasses = {Luna, Sun};
GMAT Propagator1_ForceModel.SRP = On;
GMAT Propagator1_ForceModel.RelativisticCorrection = Off;
GMAT Propagator1_ForceModel.ErrorControl = RSSStep;
GMAT Propagator1_ForceModel.GravityField.Earth.Degree = 10;
GMAT Propagator1_ForceModel.GravityField.Earth.Order = 10;
GMAT Propagator1_ForceModel.GravityField.Earth.StmLimit = 100;
GMAT Propagator1_ForceModel.GravityField.Earth.PotentialFile = 'JGM2.cof';
GMAT Propagator1_ForceModel.GravityField.Earth.TideModel = 'None';
GMAT Propagator1_ForceModel.SRP.Flux = 1367;
GMAT Propagator1_ForceModel.SRP.SRPModel = Spherical;
GMAT Propagator1_ForceModel.SRP.Nominal_Sun = 149597870.691;
GMAT Propagator1_ForceModel.Drag.AtmosphereModel = JacchiaRoberts;
GMAT Propagator1_ForceModel.Drag.HistoricWeatherSource = 'ConstantFluxAndGeoMag';
GMAT Propagator1_ForceModel.Drag.PredictedWeatherSource = 'ConstantFluxAndGeoMag';
GMAT Propagator1_ForceModel.Drag.CSSISpaceWeatherFile = 'SpaceWeather-All-v1.2.txt';
GMAT Propagator1_ForceModel.Drag.SchattenFile = 'SchattenPredict.txt';
GMAT Propagator1_ForceModel.Drag.F107 = 150;
GMAT Propagator1_ForceModel.Drag.F107A = 150;
GMAT Propagator1_ForceModel.Drag.MagneticIndex = 3;
GMAT Propagator1_ForceModel.Drag.SchattenErrorModel = 'Nominal';
GMAT Propagator1_ForceModel.Drag.SchattenTimingModel = 'NominalCycle';
GMAT Propagator1_ForceModel.Drag.DragModel = 'Spherical';

%----------------------------------------
%---------- Propagators
%----------------------------------------

Create Propagator Propagator1;
GMAT Propagator1.FM = Propagator1_ForceModel;
GMAT Propagator1.Type = RungeKutta89;
GMAT Propagator1.InitialStepSize = 8;
GMAT Propagator1.Accuracy = 9.999999999999999e-09;
GMAT Propagator1.MinStep = 8;
GMAT Propagator1.MaxStep = 8;
GMAT Propagator1.MaxStep = 8;
GMAT Propagator1.StopIfAccuracyIsViolated = true;

%----------------------------------------
%---------- Subscribers
%----------------------------------------

Create ReportFile SatPos;
GMAT SatPos.SolverIterations = Current;
GMAT SatPos.UpperLeft = [ 0 0 ];
GMAT SatPos.Size = [ 0 0 ];
GMAT SatPos.RelativeZOrder = 0;
GMAT SatPos.Maximized = false;
GMAT SatPos.Filename = 'SatPos.txt';
GMAT SatPos.Precision = 16;
GMAT SatPos.Add = {DefaultSat.EarthMJ2000Eq.X, DefaultSat.EarthMJ2000Eq.Y, DefaultSat.EarthMJ2000Eq.Z};
GMAT SatPos.WriteHeaders = false;
GMAT SatPos.LeftJustify = On;
GMAT SatPos.ZeroFill = Off;
GMAT SatPos.FixedWidth = false;
GMAT SatPos.Delimiter = ';';
GMAT SatPos.ColumnWidth = 23;
GMAT SatPos.WriteReport = true;

Create ReportFile SunPos;
GMAT SunPos.SolverIterations = Current;
GMAT SunPos.UpperLeft = [ 0 0 ];
GMAT SunPos.Size = [ 0 0 ];
GMAT SunPos.RelativeZOrder = 0;
GMAT SunPos.Maximized = false;
GMAT SunPos.Filename = 'SunPos.txt';
GMAT SunPos.Precision = 16;
GMAT SunPos.Add = {Sun.EarthMJ2000Eq.X, Sun.EarthMJ2000Eq.Y, Sun.EarthMJ2000Eq.Z};
GMAT SunPos.WriteHeaders = false;
GMAT SunPos.LeftJustify = On;
GMAT SunPos.ZeroFill = Off;
GMAT SunPos.FixedWidth = false;
GMAT SunPos.Delimiter = ';';
GMAT SunPos.ColumnWidth = 23;
GMAT SunPos.WriteReport = true;

Create ReportFile dates;
GMAT dates.SolverIterations = Current;
GMAT dates.UpperLeft = [ 0 0 ];
GMAT dates.Size = [ 0 0 ];
GMAT dates.RelativeZOrder = 0;
GMAT dates.Maximized = false;
GMAT dates.Filename = 'dates.txt';
GMAT dates.Precision = 16;
GMAT dates.Add = {DefaultSat.UTCGregorian};
GMAT dates.WriteHeaders = false;
GMAT dates.LeftJustify = On;
GMAT dates.ZeroFill = Off;
GMAT dates.FixedWidth = false;
GMAT dates.Delimiter = ';';
GMAT dates.ColumnWidth = 23;
GMAT dates.WriteReport = true;

Create ReportFile SatLatLong;
GMAT SatLatLong.SolverIterations = Current;
GMAT SatLatLong.UpperLeft = [ 0 0 ];
GMAT SatLatLong.Size = [ 0 0 ];
GMAT SatLatLong.RelativeZOrder = 0;
GMAT SatLatLong.Maximized = false;
GMAT SatLatLong.Filename = 'Sat_LatLong.txt';
GMAT SatLatLong.Precision = 16;
GMAT SatLatLong.Add = {DefaultSat.Earth.Longitude, DefaultSat.Earth.Latitude};
GMAT SatLatLong.WriteHeaders = false;
GMAT SatLatLong.LeftJustify = On;
GMAT SatLatLong.ZeroFill = Off;
GMAT SatLatLong.FixedWidth = false;
GMAT SatLatLong.Delimiter = ';';
GMAT SatLatLong.ColumnWidth = 23;
GMAT SatLatLong.WriteReport = true;

Create ReportFile RAAN;
GMAT RAAN.SolverIterations = Current;
GMAT RAAN.UpperLeft = [ 0 0 ];
GMAT RAAN.Size = [ 0 0 ];
GMAT RAAN.RelativeZOrder = 0;
GMAT RAAN.Maximized = false;
GMAT RAAN.Filename = 'RAAN.txt';
GMAT RAAN.Precision = 16;
GMAT RAAN.Add = {DefaultSat.UTCGregorian, DefaultSat.EarthMJ2000Eq.RAAN};
GMAT RAAN.WriteHeaders = true;
GMAT RAAN.LeftJustify = On;
GMAT RAAN.ZeroFill = Off;
GMAT RAAN.FixedWidth = true;
GMAT RAAN.Delimiter = ' ';
GMAT RAAN.ColumnWidth = 23;
GMAT RAAN.WriteReport = true;


%----------------------------------------
%---------- Mission Sequence
%----------------------------------------

BeginMissionSequence;
Propagate Propagator1(DefaultSat) {DefaultSat.ElapsedSecs = 2592000};
