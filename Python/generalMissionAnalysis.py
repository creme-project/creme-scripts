
from functionsDatabase import getGMATepochformat,defineOrbitFromScilab,writeGMATscript,runMissionFromPythonInDocker,runMissionFromPython,detectEvents,detectNoEvents,findVisibility,findMaxEclipse,computeGeneralTimeVector,printException,computeBatteryConsumption,plotBatteryConsumption,computeDataQuantities,plotDataBudget,saveReport,compute_path_loss_from_distance,sepUnits,delUnits,getAltitudeMax,computeLinkBudget
from optparse import OptionParser

import os
import sys
from luplink import getSlantRange,prettyMD,getLinkBugdet

import yaml
import yamlloader
from pint import UnitRegistry
import numpy as np
u = UnitRegistry()
from os import path


if __name__ == "__main__":
    #----------------------------------------
    #                 MAIN
    #----------------------------------------

    #defaultFile = 'Data/input.yaml'
    #defaultFile = 'Data/input_600km.yaml'
    defaultFile = 'Data/input_2000km.yaml'
    runGmat = True
    #runGmat = False
    parser = OptionParser()
    #parser.add_option("-f", "--file", dest="filename",
    #                  help="write report to FILE", metavar="FILE",default=defaultInitFile)
    parser.add_option("--SSO",
                    action="store_true", dest="bSSO", default=False,
                    help="define orbit giving SSO parameters")  
    
    parser.add_option("--gui",
                    action="store_true", dest="bGUI", default=False,
                    help="display GMAT gui and wait for the user to close it")
    parser.add_option("-f", "--file", dest="filename",
                      help="input FILE, default is <"+str(defaultFile)+">", metavar="FILE",default=defaultFile) 
    '''
    parser.add_option("-i", "--initialEpoch", dest="epoch",
                    help="initial epoch (default: \"2023,01,01,00,00,00\")", metavar="FILE",default="2023,01,01,00,00,00")
    '''

    (options, args) = parser.parse_args()

    print("Loading inputs...")
    print("----------")

    #------INPUTS-------
    stream = open(options.filename, 'r')
    config = yaml.load(stream, Loader = yamlloader.ordereddict.CSafeLoader)
    stream.close()
    config_for_report = sepUnits(config)
    config = delUnits(config)

    print("Starting simulation...")
    print("----------")

    #computeOrbitography(options)
    #-------ORBITOGRAPHY------------
    # Users can either specify the orbital parameters by defining an SSO (altitude, epoch and mltan)
    # or the 6 keplerian parameters in input.yaml

    bSSO = options.bSSO
    bGUI = options.bGUI
    #epoch = options.epoch
    if bSSO:
        config_for_report = defineOrbitFromScilab(config,config_for_report)
    else:
        print("Taking orbital parameters from input file.")

    print("----------")
    if not path.isdir(config["paths"]["output_dir"]):
        os.makedirs(config["paths"]["output_dir"])

    if not path.isdir(config["paths"]["output_dir"]+"Figures"):
        os.makedirs(config["paths"]["output_dir"]+"Figures")
        

    #--------GMAT simulation--------
    print("Start GMAT calculation ...")
    #Write new script from default script (DefaultSat.script --> MissionFromPython.script)
    epoch = config["ORBIT"]["epoch"]
    initial_epoch = getGMATepochformat(epoch) 
    print(initial_epoch)
    print("duration: "+str(config['SIMULATION']['total_time']))
    writeGMATscript(config)





    #Open and run GMAT simulation with new orbits parameters
    if os.name == 'posix':     
        runMissionFromPythonInDocker(config,runGmat)      
    else:
        runMissionFromPython(config['paths']['GMAT_BIN'], bGUI)
        
    print("GMAT computation done \n ----------")
    #------------COMPUTE DATA----------------

    #Compute contact times
    contact_data = detectEvents(config['paths']['output_dir']+"ContactLocator1.txt", initial_epoch)
    #Compute eclipse times
    eclipse_data = detectEvents(config['paths']['output_dir']+"EclipseLocator1.txt", initial_epoch)
    
    #Detect if there are no events
    if detectNoEvents(eclipse_data, contact_data): 
        print("No contact or eclipse time during this period, always in mode charge")
        print("Breaking the simulation as no useful event found...")
        exit()
    
    
    visibility_data = findVisibility(config['paths']['output_dir']+'ContactLocator1.txt', contact_data[3], config['SIMULATION']['total_time'])
    max_eclipse_time = findMaxEclipse(config['paths']['output_dir']+'EclipseLocator1.txt', eclipse_data[3])
    print("Max Eclipse time [s]: "+str(max_eclipse_time))
    #Compute general time data
    xlim = int(config['SIMULATION']['total_time']*86400) # Convert days to sec
    seconds = computeGeneralTimeVector(xlim) 

    print("Compute plots ...")
    #Compute satellite modes and remaining power during simulation
    
    TotBatPower = config['POWER_BUDGET']['TotBatPower']
    SolarPower = config['POWER_BUDGET']['SolarPower']
    Pmesure = config['POWER_BUDGET']['Pmesure']
    Pvidage = config['POWER_BUDGET']['Pvidage']    
    
    print("compute Battery Consumption...")
    [mode, remaining_power] = computeBatteryConsumption(xlim, eclipse_data, contact_data, TotBatPower, SolarPower, Pmesure, Pvidage)
    print("plot Battery Consumption...")    
    battery_fig_path = plotBatteryConsumption(config,eclipse_data, contact_data, seconds, xlim, mode, remaining_power, TotBatPower)



    #Compute data budget during simulation
    try:
        TC_debit_nominal = float(config['UPLINK']['data_rate'])*0.001
        TM_debit_nominal = float(config['DOWNLINK']['data_rate'])*0.001
        [contactTC, contactTM] = computeDataQuantities(contact_data, TC_debit_nominal, TM_debit_nominal)
        data_fig_path = plotDataBudget(config,contact_data, contactTC, contactTM)
    except Exception as e:
        os.remove(battery_fig_path)
        printException(e)
        sys.exit()

    print("Done ! \n ----------")

    #Compute link budget
    #report = getLinkBugdet("Data/luplink_Creme_S.json",float(SMA) - 6378.136 , False)
    try:
        
        altitude_max = getAltitudeMax(config)
        report = getLinkBugdet(options.filename,altitude_max , False)
        stringSyntheseLinkBudget = '|Category|Data|Value|Unit|\n'
        stringSyntheseLinkBudget += '| :---: | :---: | :---: | :---: |\n'
        stringSyntheseLinkBudget += prettyMD(report)
    except Exception as e:
        os.remove(battery_fig_path)
        os.remove(data_fig_path)
        printException(e)
        sys.exit()
    
    power_output_for_report = computeLinkBudget(config)
    

    #------------SAVE REPORT----------------

    #Save general report
    print("----------")
    print("Saving report ... ")
    
    shiftName = len(config['paths']['output_dir']) 
    
    try :
        saveReport(config,config_for_report, battery_fig_path[shiftName:], data_fig_path[shiftName:], stringSyntheseLinkBudget, visibility_data, max_eclipse_time, power_output_for_report)

    except Exception as e:
        os.remove(battery_fig_path)
        os.remove(data_fig_path)
        printException(e)
        sys.exit()

    print("Done !")

    print("----------")
    print("Simulation finished !")



