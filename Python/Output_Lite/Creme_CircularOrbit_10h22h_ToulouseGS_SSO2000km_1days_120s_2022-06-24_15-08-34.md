
Mission Analysis Report
=======================

# Mission Inputs

|Category|Data|Value|Unit|
| :---: | :---: | :---: | :---: |
|SIMULATION||||
||report_name|Creme_CircularOrbit_10h22h_ToulouseGS_SSO2000km_1days_120s||
||total_time|3|days|
||dt|120|sec|
|ORBIT||||
||epoch|2023,01,01,00,00,00||
||SMA|8378.14|km|
||ECC|0||
||INC|104.85|deg|
||RAAN|250.09664|deg|
||AOP|0.0|deg|
||TA|0.0|deg|
|POWER_BUDGET||||
||TotBatPower|80|Wh|
||SolarPower|24|W|
||Pmesure|14|W|
||Pvidage|18|W|
|DATA_BUDGET||||
||TC_debit_nominal|16|kbit/s|
||TM_debit_nominal|85|kbits/s|
|CONSTANTS||||
||radius_earth|6378.136|km|
||c|299792458|m/s|
||k_dB|-228.6|dB|
|SPACECRAFT_FLIGHT_DYNAMICS||||
||SC_altitude|0|km|
|GROUNDSTATION_LOCATION||||
||GS_altitude|115|m|
||GS_minElevation|10|deg|
||GS_latitude|43.6|deg|
||GS_longitude|1.5|deg|
|DOWNLINK||||
||frequency|2.2635e9|Hz|
||data_rate|85e3|bit/s|
||required_BER_from_Modulation|10.5||
||modulation|QPSK||
||system_margin|0|dB|
|UPLINK||||
||frequency|2.086e9|Hz|
||data_rate|16e3|bit/s|
||required_BER_from_Modulation|9.6||
||modulation|PCM||
||system_margin|0|dB|
|PROPAGATION_LOSSES||||
||loss_pol|3|dB|
||loss_atm|1.1|dB|
||loss_scin|0.0|dB|
||loss_rain|0.0|dB|
||loss_cloud|0.0|dB|
||loss_si|0.0|dB|
||loss_misc|0.0|dB|
|SPACECRAFT_TRANSMITTER||||
||SC_power_tx|2|W|
||SC_ant_gain_tx|6.5|dB|
||SC_loss_cable_tx|0.2|dB|
||SC_loss_connector_tx|0.1|dB|
||SC_loss_feeder_tx|4.5|dB|
||SC_misc|0|dB|
||SC_loss_point_tx|6|dB|
|SPACECRAFT_RECEIVER||||
||SC_ant_gain_rx|6.5|dB|
||SC_eta_rx|0.92||
||SC_loss_point_rx|6|dB|
||SC_loss_cable_rx|0.2|dB|
||SC_loss_connector_rx|4.6|dB|
||SC_LNA_gain|0|dB|
||SC_T_rx|550|K|
|GROUND_STATION_RECEIVER||||
||eta_rx|0.5||
||antennaDiameter|3|m|
||GS_LNA_gain|25|dB|
||GS_T_rx|610|K|
||GS_loss_point_rx|3|dB|
||GS_loss_cable_rx|0|dB|
||GS_loss_cable_D_rx|16.9|dB|
||GS_loss_connector_rx|0.1|dB|
|GROUND_STATION_TRANSMITTER||||
||GS_power_tx|50|W|
||GS_line_loss_tx|16.9|dB|
||GS_loss_connector_tx|0.1|dB|
||GS_ant_gain_tx|34.03|dB|
||GS_loss_point_rx|3|dB|

# Power analysis
  
Max eclipse time during period = 1961.12 s / 33 min  
![PowerAnalysis](Figures/BatteryRemainingPower_2022-06-24_15-08-33.png)
# Data analysis
  
Max visibility during period = 1302.02 s / 22 min  
Min visibility during period = 92.96 s / 2 min  
Mean visibility during period = 922.68 s / 15 min  
Max visibility during a day and during period = 7721.61 s / 129 min  
Min visibility during a day and during period = 6515.53 s / 109 min  
Mean visibility per day during period = 7073.92 s / 118 min  
![DataAnalysis](Figures/DataBudget_2022-06-24_15-08-33.png)
# Link analysis
  
|Category|Data|Value|Unit|
| :---: | :---: | :---: | :---: |
|Downlink||||
|Tx|||
||SC_power_tx (W)|2.0|dB|
||SC_power_tx|3.0102999566398116|dB|
||gain_tx_dB|6.5|dBi|
||SC_EIRP_TX|9.510299956639813|dB|
||tx_losses|10.8|dB|
||EIRP depointing|-1.2897000433601882|dB|
|Path|||
||k_dB|-228.6|dB|
||path_loss|176.5846924484078|dB|
|Rx|||
||T_r (K)|610.0|dB|
||T_r_dB (dB)|27.853298350107668|dB|
||gain_r (dBi)|34.034318509702985|dB|
||G/T_rx (dB)|6.181020159595317|dB|
||LNA rx (dB)|25.0|dB|
||total gain (dB)|34.034318509702985|dB|
||rx_losses|3.0|dB|
|Synthesis|||
||TOT losses|190.3846924484078|dB|
||C/N0 Downlink|53.90662766782732|dB|
||system_margin (dB)|0.0|dB|
||data_rate (bps)|85000.0|dB|
||data_rate (dB)|49.29418925714292|dB|
||Eb_N0 downlink|4.612438410684405|dB|
||Eb/N0 Threshold|10.5|dB|
||Downlink margin|-5.887561589315595|dB|
|Uplink||||
|Tx|||
||P_t (W)|50.0|dB|
||P_t (dB)|16.989700043360184|dB|
||gain_tx_dB (dB)|34.03|dB|
||GS_EIRP_TX |51.01970004336019|dB|
||tx_losses|20.0|dB|
||EIRP depointing |31.01970004336019|dB|
|Path|||
||k_dB (dB)|-228.6|dB|
||path_loss|176.5846924484078|dB|
|Rx|||
||T_r (K)|550.0|dB|
||T_r_dB (dB)|27.403626894942434|dB|
||rx_losses|6.0|dB|
||Antenna gain (dBi)|6.5|dB|
||LNA rx (dB)|0.0|dB|
||total gain (dB)|6.5|dB|
|Synthesis|||
||TOT losses|202.5846924484078|dB|
||C/N0 Uplink|56.131380700009956|dB|
||data_rate (bps)|16000.0|dB|
||data_rate (dB)|42.04119982655924|dB|
||Eb_N0 uplink|14.090180873450713|dB|
||Uplink margin|4.490180873450713|dB|

|Category|Data|Value|Unit|
| :---: | :---: | :---: | :---: |
|UPLINK POWER||||
||Minimal power at duplexer input|-119.15566851023245|dBm|
||Maximal power at duplexer input|-99.13498654537496|dBm|
|DOWNLINK_POWER||||
||Minimal power at SATCORE input|-108.84439249176802|dBm|
||Maximal power at SATCORE input|-88.82371052691049|dBm|
